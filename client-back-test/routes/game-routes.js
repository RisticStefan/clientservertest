import express from 'express'
import { loadLuckySixGame, logIn, payIn, payOut, test } from '../controllers/luckySixController.js'

const router = express.Router();

router.post('/lucky-six', loadLuckySixGame)
router.post('/login', logIn)
router.post('/pay-in', payIn)
router.post('/pay-out', payOut)
router.get('/test', test)

export default router